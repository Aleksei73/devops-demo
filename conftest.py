from datetime import datetime
import os
import allure
import pytest
from selenium import webdriver

desiredCapabilities={
"browserName":"chrome"
}

host = os.environ.get('MAIN_HOST')
port = os.environ.get('MAIN_PORT')

@pytest.fixture(scope='function')
def driver():
    options = webdriver.ChromeOptions()
    options.add_argument("--headless")
    driver = webdriver.Remote(command_executor=f'http://{host}:{port}/wd/hub',desired_capabilities = desiredCapabilities)
    yield driver
    driver.quit()
